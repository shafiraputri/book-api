from pydantic import BaseModel
from typing import Optional
from fastapi import FastAPI, HTTPException, UploadFile, File
from starlette.responses import FileResponse
from fastapi.responses import StreamingResponse
import os
import aiofiles as aiofiles

app = FastAPI()

isdir = os.path.isdir("bookFiles") 
if not isdir:
    os.makedirs("bookFiles")

booksDB = {}

BUKU_DENGAN_ID_STR = "Buku dengan id"
BOOK_FILES_DIR = "bookFiles/"

class Book(BaseModel):
    id: int
    judul: str
    penulis: str
    penerbit: str
    tahunTerbit: int
    isbn: str
    sinopsis: Optional[str] = None
    
@app.get("/books-api/")
def get_book():
    return booksDB

@app.get("/books-api/{id}/")
def get_book(id:int):
    try:
      return booksDB[id]
    except KeyError:
      raise HTTPException(status_code=400, detail= BUKU_DENGAN_ID_STR + " '" + str(id) + "' tidak ada")

@app.post("/books-api/")
def post_book(book: Book):
    if book.id not in booksDB : 
        new_book = Book(
            id = book.id, 
            judul = book.judul, 
            penulis = book.penulis, 
            penerbit = book.penerbit, 
            tahunTerbit = book.tahunTerbit, 
            isbn = book.isbn, 
            sinopsis = book.sinopsis)
        booksDB[book.id] = new_book
        return new_book
    else :
        raise HTTPException(status_code=400, detail= BUKU_DENGAN_ID_STR + " '" + str(book.id) + "' sudah ada")

@app.delete("/books-api/{id}/")
def delete_book(id:int):
    try : 
        deleted_book = booksDB.pop(id)
        return {"message" : "Successfully deleted expense", "deleted_book" : deleted_book}
    except KeyError:
        raise HTTPException(status_code=400, detail= BUKU_DENGAN_ID_STR + " '" + str(id) + "' tidak ada")

@app.put("/books-api/{id}/")
def update_book(id: int, book_data: Book):
    try : 
        book = booksDB[id]
        book.judul = book_data.judul
        book.penulis = book_data.penulis
        book.penerbit = book_data.penerbit
        book.tahunTerbit = book_data.tahunTerbit
        book.isbn = book_data.isbn
        book.sinopsis = book_data.sinopsis
        return book
    except KeyError:
        raise HTTPException(status_code=400, detail= BUKU_DENGAN_ID_STR + " '" + str(id) + "' tidak ada")

@app.post("/books-api/upload-file/")
async def upload_file(file: UploadFile = File(...)):
    async with aiofiles.open(f"bookFiles/{file.filename}", 'wb') as fileout:
        content = await file.read()
        await fileout.write(content)
    return {"message" : "Sukses mengunggah file bernama '" + str(file.filename) + "'."}

@app.get("/books-api/get-files/")
def get_files():
    list_dir = os.listdir(BOOK_FILES_DIR)
    return {"list-files" : list_dir}

@app.get("/books-api/get-file/{file_name}/")
def get_file(file_name: str):
    isfile = os.path.isfile(BOOK_FILES_DIR + file_name) 
    if isfile:
        return FileResponse(BOOK_FILES_DIR + file_name, media_type='application/octet-stream',filename=file_name)
    else:
        return {"message" : "File dengan nama '" + file_name + "' tidak ada."}
